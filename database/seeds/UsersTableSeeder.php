<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Admin Admin',
            'username' => 'admin_black',
            'email' => 'admin@black.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('wallets')->insert([
            'id' => 1, 
            'holder_type' => 'App\\User', 
            'holder_id' => 1, 
            'name' => 'Default Wallet', 
            'slug' => 'default', 
            'description' => NULL, 
            'balance' => 20000000, 
            'decimal_places' => 2, 
            'created_at' => '2019-09-01 07:49:27', 
            'updated_at' => '2019-09-01 08:00:02'
        ]);
    }
}
