<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class TransferRequest extends FormRequest
{
    // change amount value
    protected function prepareForValidation()
    {
        if ($this->has('amount')){
            $this->merge(['amount'=>(int)str_replace(',', '', $this->amount)]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => [
                'required', 'string', 'max:255', 'regex:/^\S*$/u', 'exists:users,username', 
                'not_in:'.auth()->user()->username
            ],
            'amount' => [
                'required', 'integer', 'lte:'.auth()->user()->balance
            ]
        ];
    }
}
