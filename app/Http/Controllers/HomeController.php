<?php

namespace App\Http\Controllers;
use App\Transfer;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $transfer = Transfer::select(\DB::raw('SUM(transactions.amount) AS total_transfer'))
            ->leftJoin('transactions', 'transfers.deposit_id', '=', 'transactions.id')
            ->where('from_id', auth()->id())
            ->groupBy('transfers.from_id')
            ->first();
        return view('dashboard')->with([
            'total_transfer' => (isset($transfer->total_transfer)?$transfer->total_transfer:0)
        ]);
    }
}
