<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transfers';

    /**
     * Get from user.
     */
    public function from_user()
    {
        return $this->belongsTo('App\User', 'from_id');
    }

    /**
     * Get to user.
     */
    public function to_user()
    {
        return $this->belongsTo('App\User', 'to_id');
    }

    /**
     * Get deposit detail.
     */
    public function deposit()
    {
        return $this->belongsTo('App\Transaction', 'deposit_id');
    }

    /**
     * Get withdraw detail.
     */
    public function withdraw()
    {
        return $this->belongsTo('App\Transaction', 'withdraw_id');
    }
}
