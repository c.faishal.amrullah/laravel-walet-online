@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Total Saldo</h5>
                    <h3 class="card-title">
                        <i class="tim-icons icon-money-coins text-info"></i>&nbsp;{{number_format(auth()->user()->balance, 0)}}
                    </h3>
                </div>
            </div>
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Total Transfer</h5>
                    <h3 class="card-title">
                        <i class="tim-icons icon-double-right text-primary"></i>
                        {{ number_format((isset($total_transfer)?$total_transfer:"0")) }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="card ">
                <div class="card-header">
                    <h4 class="card-title">Lastest History Transaction</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table tablesorter" id="">
                            <thead class=" text-primary">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Date</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th class="text-center">Amount</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        var i=0;
        function table_ajax(page){
            $('.table-body').html('<tr><td class="text-center" colspan="6"><div class="loader" class="text-center"></div></td></tr>');
            var request = $.ajax({
                url: '{{ route("transaction.index")}}',
                data:{
                    user_id: '',
                    page: page
                },
                success: function (response) {
                    i++;
                    if(i > 1){
                        var html = '';
                        $('.table-body').html(html);
                        for (let index = 0; index < response.data.length; index++) {
                            html = '<tr>';
                            html += '<td class="text-center">'+(index+1)+'</td>';
                            html += '<td>'+response.data[index].created_at+'</td>';
                            html += '<td>'+response.data[index].from_user.name+'</td>';
                            html += '<td>'+response.data[index].to_user.name+'</td>';
                            html += '<td class="text-center">'+response.data[index].deposit.amount.toLocaleString()+'</td>';
                            html += '<td>Transfer</td>';
                            html += '</tr>';
                            $('.table-body').append(html);
                        }
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }
        table_ajax(1);
    </script>
@endpush
