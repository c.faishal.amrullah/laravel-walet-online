@extends('layouts.app', ['page' => __('Transaction Histories'), 'pageSlug' => 'transaction'])

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title">Transaction Table</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter" id="">
                        <thead class=" text-primary">
                            <tr>
                                <th class="text-center">#</th>
                                <th>Date</th>
                                <th>From</th>
                                <th>To</th>
                                <th class="text-center">Amount</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody class="table-body">
                        </tbody>
                    </table>
                </div>
                <ul class="nav pull-right">
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">page :</a>
                    </li>
                    <div class="table-page" style="display:flex;">
                    </div>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        var i=0;
        function table_ajax(page){
            $('.table-body').html('<tr><td class="text-center" colspan="6"><div class="loader" class="text-center"></div></td></tr>');
            var request = $.ajax({
                url: '{{ route("transaction.index")}}',
                data:{
                    user_id: '',
                    page: page
                },
                success: function (response) {
                    i++;
                    if(i > 1){
                        var html = '';
                        var disabled_page = '';
                        $('.table-body').html(html);
                        for (let index = 0; index < response.data.length; index++) {
                            html = '<tr>';
                            html += '<td class="text-center">'+(index+1)+'</td>';
                            html += '<td>'+response.data[index].created_at+'</td>';
                            html += '<td>'+response.data[index].from_user.name+'</td>';
                            html += '<td>'+response.data[index].to_user.name+'</td>';
                            html += '<td class="text-center">'+response.data[index].deposit.amount.toLocaleString()+'</td>';
                            html += '<td>Transfer</td>';
                            html += '</tr>';
                            $('.table-body').append(html);
                        }
                        html = '';
                        $('.table-page').html(html);
                        for (let index = 1; index <= parseInt(response.last_page); index++) {
                            disabled_page = '" onclick="table_ajax('+index+')"';
                            if(index == parseInt(response.current_page)){
                                disabled_page = 'disabled"';
                            }
                            html = '<li class="nav-item">';
                            html += '<a class="nav-link '+disabled_page+' href="#">'+index+'</a>';
                            html += '</li>';
                            $('.table-page').append(html);
                        }
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }
        table_ajax(1);
    </script>
@endpush
