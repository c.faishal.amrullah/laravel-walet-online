@extends('layouts.app', ['page' => __('Transfer'), 'pageSlug' => 'transfer'])

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">{{ _('Transfer Form') }}</h5>
                </div>
                <form method="post" action="{{ route('transfer.store') }}" autocomplete="off">
                    <div class="card-body">
                        @csrf
                        @include('alerts.success')

                        <div class="form-group">
                            <label>{{ _('Username') }}</label>
                            <div class="input-group{{ $errors->has('username') ? ' has-danger' : '' }}">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="tim-icons icon-badge"></i>
                                    </div>
                                </div>
                                <input type="text" name="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" 
                                    placeholder="{{ _('Username') }}" value="{{ old('username') }}" onkeyup="search_username($(this))">
                                @include('alerts.feedback', ['field' => 'username'])
                            </div>
                        </div>

                        <div class="form-group">
                            <small class="text-muted search-progress" style="display:none;">search username<span class="progress-dot">...</span></small>
                            <ul class="nav nav-search" style="display:none;"></ul>
                        </div>

                        <div class="form-group">
                            <label>{{ _('Amount') }}</label>
                            <div class="input-group{{ $errors->has('amount') ? ' has-danger' : '' }}">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="tim-icons icon-money-coins"></i>
                                    </div>
                                </div>
                                <input type="text" name="amount" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" 
                                    placeholder="{{ _('0') }}" value="{{ old('amount') }}">
                                @include('alerts.feedback', ['field' => 'amount'])
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary">{{ _('Transfer Wallet') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        var username_progress = false;
        var request;
        var i = 0;
        var timer;

        function search_username(elm){
            $('.search-progress').css('display', 'block');
            $('.nav-search').css('display', 'none');
            if(username_progress){
                // stop ajax and inteval
                request.abort();
                clearInterval(timer);
            }
            username_progress = true;
            i = 0;
            timer = setInterval(function() {
                i = ++i % 5;
                $(".progress-dot").html(Array(i+1).join("."));
            }, 800);
            request = $.ajax({
                url: '{{ route("user.index")}}/'+elm.val()+'/find',
                type: "POST",
                data:{_token: '{{ csrf_token() }}'},
                success: function (response) {
                    var html = '';
                    for (let index = 0; index < response.length; index++) {
                        html += '<li class="nav-item"><a class="nav-link" onclick="set_username($(this))" href="#" data-value="'+response[index]+'">'+response[index]+'</a></li>';
                    }
                    $('.search-progress').css('display', 'none');
                    $('.nav-search').html(html);
                    $('.nav-search').css('display', 'flex');
                    request.abort();
                    clearInterval(timer);
                },
                error: function (e) {
                    // console.log(e);
                }
            });
        }

        function set_username(elm){
            $('input[name="username"]').val(elm.attr('data-value'));
        }

        $('input[name="amount"]').on('change', function(){
            var number = $(this).val();
            number = number.replace(/\,/g,'');
            if(parseInt(number)){
                number = parseInt(number);
            }else{
                number = parseInt("0");
            }
            if(number == '0'){ 
                number = ''; 
            }else{
                number = number.toLocaleString()
            }
            $(this).val(number);
        });
    </script>
@endpush