<footer class="footer">
    <div class="container-fluid">
        <ul class="nav">
            <li class="nav-item">
                <a href="https://fb.me/vhal.skyland" target="blank" class="nav-link">
                    {{ _('Facebook') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="https://linkedin.com/in/d2gg-jx" target="blank" class="nav-link">
                    {{ _('LinkedIn') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="https://instagram.com/jinrave" target="blank" class="nav-link">
                    {{ _('Instagram') }}
                </a>
            </li>
        </ul>
        <div class="copyright">
            &copy; {{ now()->year }} {{ _('made with') }} <i class="tim-icons icon-heart-2"></i> {{ _('by') }}
            <a href="mailto:c.faishal.amrullah@gmail.com" target="_blank">{{ _('Jx') }}</a> {{ _('for simple wallet online') }}.
        </div>
    </div>
</footer>
