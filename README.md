
## LARAVEL-WALLET-ONLINE
Wallet online use laravel framework

## Installation

Initial project

> Clone repository and create new repository for your project.

```bash 
foo@bar:~$ cp .env.example .env 
```

## Setup your connection

```bash
DB_CONNECTION=mysql
DB_HOST="remotemysql.com"
DB_PORT=3306
DB_DATABASE="hWuKIw7Qqk"
DB_USERNAME="hWuKIw7Qqk"
DB_PASSWORD="hV6qJUy1mS"
```

```bash
foo@bar:~$ composer install
foo@bar:~$ php artisan key:generate
foo@bar:~$ php artisan migrate
foo@bar:~$ php artisan db:seed
```

## Feature

Simple Register & Login
> We've created simple registration for testing purpose only.

Fast Transfer
> We've developed fast transfer feature.

Dashboard and History Transaction
> There is dashboard and history transaction.

User Profile
> Feature for editing user detail.


## Mode
1. light mode 
2. dark mode

## Sidebar Background
1. purple
2. blue
3. green

## License
[MIT](LICENSE)